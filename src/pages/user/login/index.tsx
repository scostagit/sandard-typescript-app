import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchLoginStart } from '../../../store/actions';
import IUserState from '../../../interfaces/states/IUserState';
import { LoadingComponent, ErrorMessage, Field, useForm } from '../../../components';
import './Login.css';

const Login = (): JSX.Element => {
  const userState: IUserState = useSelector((state: any) => state.usersReducer);
  const dispatch = useDispatch();
  const onSubmit = (values: any, erros: any): void => {
    if (erros === null) {
      dispatch(fetchLoginStart({
        values
      }));
    }
  };

  function validate(values: any): void {
    const errors: any = {};
    if (values.userName === '') {
      errors.userName = 'Required';
    } else if (values.userName.length > 15) {
      errors.userName = 'max length 11';
    } else {
      errors.userName = '';
    }

    if (values.password === '') {
      errors.password = 'Required';
    } else if (values.password.length > 20) {
      errors.password = 'max length 20';
    } else {
      errors.password = '';
    }

    return errors;
  }

  const { values, onBlur, onChange, handleSubmit, errors } = useForm({
    initialValues: {
      userName: '',
      password: ''
    },
    onSubmit,
    validate
  });

  return (
    <form id="frmLogin" onSubmit={handleSubmit}>
      <div className="login-box" id="homepage-login-box">
        <div className="login-box-body">
          <div className="login-box-content">
            <Field
              type="text"
              name="userName"
              label="Username"
              value={values.userName}
              onChange={onChange}
              onBlur={onBlur}
              error={errors.userName}
            />
            <Field
              type="password"
              name="password"
              label="Password"
              value={values.name}
              onChange={onChange}
              onBlur={onBlur}
              error={errors.password}
            />
          </div>
          <LoadingComponent display={userState.loading} />
          <div className="clear" />
          <ErrorMessage
            display={userState.errorMessage !== ''}
            message={userState.errorMessage}
          />
          <div className="browserErr" id="browserErr" />
          <div className="buttonWrpr" id="buttonWrpr">
            <div className="buttons">
              <button id="btnLogin" type="submit" disabled={userState.loading}>Login</button>
            </div>
          </div>
        </div>
      </div>
    </form>
  );
};

export default Login;
