import React from 'react';
import renderer from 'react-test-renderer';
import Login from './index';
import { shallow, mount } from 'enzyme';
import { testStore } from '../../../utils/testStore';
import { Provider } from 'react-redux';

let store = null;
let wrapper: any;
let wrapperMount: any;
let loginComponent: any;

describe('Test Login Component', () => {
    beforeAll(() => {
        store = testStore({});
        loginComponent = (
            <Provider store={store}>
                <Login />
            </Provider>
        );
        wrapper = shallow(loginComponent);
        wrapperMount = mount(loginComponent);
    });

    describe('Test Login Component render', () => {
        it('should have not changes', () => {
            const component = renderer.create(wrapper.instance());
            let tree = component.toJSON();
            expect(tree).toMatchSnapshot();
        });

        it('should handle the submit event', () => {
            wrapperMount.find('form#frmLogin').simulate('submit');
        });

        it('should have filled username and password', () => {
            //console.log(wrapperMount.debug());
            expect(wrapperMount.find('#userName').props()).toEqual(
                expect.objectContaining({ name: 'userName' })
            );
        });
    });

    describe('User signin', () => {
        it('should fail if no credentials are provided', () => {
            const fakeEvent = { preventDefault: () => { } };
            expect(wrapperMount.find('#frmLogin').length).toBe(1);
            wrapperMount.find('#frmLogin').simulate('submit', fakeEvent);
        });

        it('should disable submit button on submit click', () => {
            const submitButton = wrapperMount.find('button#btnLogin');
            submitButton.simulate('click');
            expect(submitButton.prop('disabled')).toEqual(false);
        });

        it('2 plus 2 should be 4', () => {
            expect(2 + 2).toEqual(4);
        });
    });
});
