import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchJobsStart } from '../../store/actions';
import Search from './components/Search';
import { LoadingComponent } from '../../components';
import IJobsState from '../../interfaces/states/IJobsState';
import JobModel from '../../models/JobModel';

const JobPage = () => {
    const jobsState: IJobsState = useSelector((state: any) => state.jobsReducer);

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchJobsStart());
    }, []);

    return jobsState.loading ? (<LoadingComponent display={jobsState.loading} />) : (
        <div>
            <h2>Job Page 2</h2>
            <Search />
            {
                <ul>
                    {jobsState.jobs.map((j: JobModel) => (
                        <li key={j.eFolderID}>{j.eFolderName} - {j.txtSupplierName}</li>
                    ))}
                </ul>
            }
        </div>
    );
};

export default JobPage;
