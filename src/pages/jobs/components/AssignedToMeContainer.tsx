import * as React from 'react';

interface IAssignedToMeContainerProps {
    toDoListColumnHeaderAssignedToMeText?: string;
}

const AssignedToMeContainer = ({ toDoListColumnHeaderAssignedToMeText = 'Assigned To Me' }: IAssignedToMeContainerProps) => (
    <div>
        <span id="AssignedToMeContainer">
            <label
                htmlFor="AssignedToMe"
                data-translation-id="ViewPage.ViewPage_ToDoList_ColumnHeader_AssignedToMe_Text"
            >
                {toDoListColumnHeaderAssignedToMeText}
            </label>
            <input id="AssignedToMe" name="AssignedToMe" type="checkbox" />
            <label htmlFor="AssignedToMe" className="ui-checkbox">
                <span></span>
            </label>
        </span>
    </div>
);

export default AssignedToMeContainer;