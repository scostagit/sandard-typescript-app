import * as React from 'react';
import AssignedToMeContainer from './AssignedToMeContainer';
import JobStatusContainer from './JobStatusContainer';

interface ISearchProps {
    placeholder?: string;
    translatorButtonSubmitText?: string;
    translatorButtonResetText?: string;
    assignedToMeFilter?: boolean;
    jobStatusFilter?: boolean;
    //onClick(e: React.MouseEvent<HTMLElement>): void;
}

const Search = ({
    placeholder = 'Quick Sarch',
    translatorButtonSubmitText = 'Search',
    translatorButtonResetText,
    assignedToMeFilter = true,
    jobStatusFilter = true
}: ISearchProps) => {

    const handleClick = (e: React.MouseEvent<HTMLElement>): void => {
        e.preventDefault();
    };

    return (
        <div id="search">
            <span id="searchFieldContainer">
                <input
                    type="text"
                    id="searchField"
                    name="searchField"
                    placeholder={placeholder}
                    data-translation-id="ViewPage.ViewPage_QuickSearch_Text"
                />
                <button
                    type="button"
                    id="btnSearch"
                    onClick={handleClick}
                    data-translation-id="WebResources.ViewPage_Button_Submit_Text"
                >
                    {translatorButtonSubmitText}
                </button>
                <button
                    type="button"
                    id="btnReset"
                    className="hidden"
                    data-translation-id="WebResources.ViewPage_Button_Reset_Text"
                >
                    {translatorButtonResetText}
                </button>
            </span>
            {assignedToMeFilter ? <AssignedToMeContainer /> : null}

            {jobStatusFilter ? <JobStatusContainer /> : null}
        </div>
    );
};


export default Search;
