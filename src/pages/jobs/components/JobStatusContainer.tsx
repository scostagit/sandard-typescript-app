import * as React from 'react';

interface IJobStatusContainerPros {
    toDoListColumnHeaderJobStatusText?: string;
}

const JobStatusContainer = ({ toDoListColumnHeaderJobStatusText = 'Job Status' }: IJobStatusContainerPros) => (
    <div>
        <span id="JobStatusContainer">
            <label
                htmlFor="JobStatus"
                data-translation-id="ViewPage.ViewPage_ToDoList_ColumnHeader_JobStatus_Text"
            >
                {toDoListColumnHeaderJobStatusText}
            </label>
            <label className="widget-wrapper">
                <select id="JobStatus" name="JobStatus"></select>
            </label>
        </span>
    </div>
);

export default JobStatusContainer;