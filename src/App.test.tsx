import * as React from 'react';
import { render } from 'react-dom';
import renderer from 'react-test-renderer';
import App from './App';
import { testStore } from './utils/testStore';
import Login from './pages/user/login';
import * as enzyme from 'enzyme';
import { Provider } from 'react-redux';

const store = testStore({});

describe('Test App Component', () => {

  const AppComponent: JSX.Element = (
    <Provider store={store}>
      <App />
    </Provider>);

  beforeEach(() => {

  });

  it('renders without crashing', () => {
    const div = document.createElement('div');
    render(AppComponent, div);
    /* const component = findByTestAtrr(wrapper, 'appComponent');
    expect(component.length).toBe(1);*/
  });

  it('True and false', () => {
    expect(true).toEqual(true);
  });

  it('Snapshot randers', () => {
    const component = renderer.create(AppComponent);
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('renders Hello', () => {
    const wrapperHello = enzyme.shallow(
      <div>
        <h1>Hello, Enzyme!</h1>
      </div>);
    expect(wrapperHello.find('h1').html()).toMatch(/Hello, Enzyme/);
  });

  it('renders the inner Login', () => {
    const wrapperMount = enzyme.mount(AppComponent);
    expect(wrapperMount.find(Login).length).toEqual(1);
  });

  it('should render correctly in "debug" mode', () => {
    const wrapper = enzyme.shallow(AppComponent);
    expect(wrapper).toMatchSnapshot();
  });
});
