import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { createLogger } from 'redux-logger';
import rootReducers from './reducers';
import { rootSaga } from './sagas';

const loggerMiddleware = createLogger();

// create the saga middleware
const sagaMiddleware = createSagaMiddleware();

const store = createStore(
    rootReducers,
    applyMiddleware(sagaMiddleware, loggerMiddleware)
);

// then run the saga
sagaMiddleware.run(rootSaga);

export default store;
