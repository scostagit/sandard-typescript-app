import { jobsConstants } from '../constants';
import IJobsState from '../../interfaces/states/IJobsState';

const initialState: IJobsState = {
    loading: false,
    jobs: [],
    errorMessage: ''
};

export function jobsReducer(state: IJobsState = initialState, action: any): IJobsState {
    switch (action.type) {
        case jobsConstants.FETCH_JOBS_START: {
            return {
                ...state,
                loading: true,
                jobs: [],
                errorMessage: ''
            };
        }
        case jobsConstants.FETCH_JOBS_SUCCESS: {
            return {
                ...state,
                loading: false,
                jobs: action.payload
            };
        }
        case jobsConstants.FETCH_JOBS_FAILURE: {
            return {
                ...state,
                loading: false,
                jobs: [],
                errorMessage: action.payload
            };
        }

        default: {
            return state;
        }
    }
}
