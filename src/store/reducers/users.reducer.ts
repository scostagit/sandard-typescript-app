import IUserState from '../../interfaces/states/IUserState';
import { userConstants } from '../constants';

const initialState: IUserState = {
    loading: false,
    user: {},
    errorMessage: ''
};

export function usersReducer(state: IUserState = initialState, action: any): IUserState {
    switch (action.type) {
        case userConstants.FETCH_LOGIN_REQUEST:
            return {
                ...state,
                loading: true,
                user: {
                    userName: action.payload.userName,
                    password: action.payload.password
                },
                errorMessage: ''
            };

        case userConstants.FETCH_LOGIN_SUCCESS: {
            if (action.payload.success) {
                return {
                    ...state,
                    loading: true,
                    errorMessage: '',
                    user: {
                        userName: 'coste',
                        name: 'sergio',
                        email: 'test@test.com',
                        isLogged: true
                    }
                };
            } else {
                return {
                    ...state,
                    loading: false,
                    user: {},
                    errorMessage: action.payload.message
                };
            }
        }
        case userConstants.FETCH_LOGIN_FAILURE:
            return {
                ...state,
                loading: false,
                user: {},
                errorMessage: action.payload
            };

        default:
            {
                return state;
            }

    }
}
