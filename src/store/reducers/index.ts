
import { combineReducers } from 'redux';
import { usersReducer } from './users.reducer';
import { jobsReducer } from './jobs.reducer';

const rootReducers = combineReducers({
    usersReducer,
    jobsReducer
});

export default rootReducers;