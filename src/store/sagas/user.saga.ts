import { call, put, takeLatest, select } from 'redux-saga/effects';
import { AuthApi } from '../../api';
import { userConstants } from '../constants/';
import { fetchLoginSuccess, fetchLoginFailure, logoutStart } from '../actions';

function* login(action: any) {
    try {
        const user = yield call(() => AuthApi.login(action.payload.userName, action.payload.password)
            .then(response => response));

        yield put(fetchLoginSuccess(user));
    }
    catch (error) {
        yield put(fetchLoginFailure(error));
    }
}

export function* userLoginSaga() {
    // const user = (state:any) => state.user;
    // const loginUser = yield select(user);
    //yield takeLatest(userConstants.FETCH_LOGIN_REQUEST, login, loginUser);
    yield takeLatest(userConstants.FETCH_LOGIN_REQUEST, login);
}

export function* logoutSaga() {
    yield put(logoutStart());
}

