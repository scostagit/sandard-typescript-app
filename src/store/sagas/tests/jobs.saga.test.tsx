import { call, put, select, takeLatest } from 'redux-saga/effects';
import { cloneableGenerator } from '@redux-saga/testing-utils';
import { fetchJobsSaga } from '../jobs.saga';
import { fetchLJobsSuccess, fetchJobsFailure } from '../../actions';
import { jobsConstants } from '../../constants';

describe('jobs saga native testing', () => {
  const generator = cloneableGenerator(fetchJobsSaga)();
  const jobs = [{
    eFolderID: '0900007000000000000000002502716',
    eFolderName: 'LPFR-UAT-1044344',
    eCreationTime: 'Thu Oct 25 2019 11:23:00 GMT+0100 (British Summer Time)',
    eEntryTime: 'Thu Oct 25 2019 11:23:00 GMT+0100 (British Summer Time)',
    txtPRNNumber: '5566f0fc-e9be-484c-95d7-34c5a70aabc0',
    txtVehicleRegistrationNumber: 'DP778WJ',
    txtVehicleDriverName: 'SAMUEL FLORIAN',
    txtVehicleModel: 'A4 AVANT BUSINE',
    txtSupplierName: 'A.I.G.',
    txtDropOffTime: '09:00',
    txtVehicleVINNumber: 'WAUZZZ8K1FA105332'
  }];

  it('forks fetch jobs start', () => {
    const result = generator.next().value;

    expect(result.payload.args).toContain(jobsConstants.FETCH_JOBS_START);
    //expect(generator.next(jobs).value).toEqual(call(JobApi.getAll));
  });

  describe.skip('and the request is successful', () => {
    let clone: any;

    beforeAll(() => {
      clone = generator.clone();
    });

    it('raises success action', () => {
      const result = clone.next(jobs).value;
      expect(result).toEqual(put(fetchLJobsSuccess(jobs)));
    });

    it('performs no further work', () => {
      const result = clone.next().done;
      expect(result).toBe(true);
    });
  });

  describe.skip('and the request fails', () => {
    let clone: any;

    beforeAll(() => {
      clone = generator.clone();
    });

    it('raises failed action', () => {
      const error = '404 Not Found';
      const result = clone.next().value.throw(error).value;
      expect(result).toEqual(put(fetchJobsFailure(error)));
    });

    it('performs no further work', () => {
      const result = clone.next().done;
      expect(result).toBe(true);
    });
  });
});
