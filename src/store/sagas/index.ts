import { all, fork } from 'redux-saga/effects';
import { fetchJobsSaga } from './jobs.saga';
import { userLoginSaga, logoutSaga } from './user.saga';

export function* rootSaga() {
    yield all([fork(fetchJobsSaga)]);
    yield all([fork(userLoginSaga)]);
    yield all([fork(logoutSaga)]);
}