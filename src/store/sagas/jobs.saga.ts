import { call, put, takeLatest } from 'redux-saga/effects';
import { jobsConstants } from '../constants';
import { fetchLJobsSuccess, fetchJobsFailure } from '../actions';
import { JobApi } from '../../api';

export function* fetchJobsSaga() {
    yield takeLatest(jobsConstants.FETCH_JOBS_START, fetchJobs);
}

function* fetchJobs() {
    try {
        const jobs = yield call(() => JobApi.getAll()
            .then(response => response));

        yield put(fetchLJobsSuccess(jobs.data));

    }
    catch (error) {
        yield put(fetchJobsFailure(error));
    }
}