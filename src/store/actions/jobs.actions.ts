import { jobsConstants } from '../constants/';
import JobModel from '../../models/JobModel';

export const fetchJobsStart = () => ({
    type: jobsConstants.FETCH_JOBS_START,
    payload: null,
})

export const fetchLJobsSuccess = (jobs: JobModel[]) => ({
    type: jobsConstants.FETCH_JOBS_SUCCESS,
    payload: jobs
});

export const fetchJobsFailure = (error: any) => ({
    type: jobsConstants.FETCH_JOBS_SUCCESS,
    payload: error
});

