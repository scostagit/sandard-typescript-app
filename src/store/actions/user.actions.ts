import { userConstants } from '../constants/';
import UserModel from '../../models/UserModel';

export const fetchLoginStart = (userInfo: any) => ({
    type: userConstants.FETCH_LOGIN_REQUEST,
    payload: userInfo,
})

export const fetchLoginSuccess = (user: UserModel) => ({
    type: userConstants.FETCH_LOGIN_SUCCESS,
    payload: user
});

export const fetchLoginFailure = (error: any) => ({
    type: userConstants.FETCH_LOGIN_FAILURE,
    payload: error
});

export const logoutStart = () => ({
    type: userConstants.FETCH_LOGOUT,
    payload: null
});

