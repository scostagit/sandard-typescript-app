import axios from 'axios';
import { omit } from 'ramda';

//let API_HOST;

// switch (process.env.REACT_APP_RUN) {
//   case 'local':
//     API_HOST = '/';
//     break;
//   default:
//     API_HOST = 'sims/';
// }

//const API_VERSION = ''; //'v1';
const API_URL = ''; //`${API_HOST}/api/${API_VERSION}`;

const defaultOptions = {
    withCredentials: true,
    baseURL: API_URL,
    crossdomain: true
};

const requests = {
    post: (url: any, body: any, customOptions = {}) => {
        let options = { ...defaultOptions, ...customOptions };
        return axios({
            ...options,
            method: 'post',
            url: url,
            data: body
        });
    },

    get: (url: any, body?: any, customOptions = {}) => {
        let options = { ...defaultOptions, ...customOptions };
        return axios({
            ...options,
            method: 'get',
            url: url,
            data: body
        });
    },

    delete: (url: any, customOptions = {}) => {
        let options = { ...defaultOptions, ...customOptions };
        return axios({
            ...options,
            method: 'delete',
            url: url
        });
    },

    put: (url: any, body: any, customOptions = {}) => {
        let options = { ...omit(['baseURL'], defaultOptions), ...customOptions };
        return axios({
            ...options,
            method: 'put',
            data: body,
            url
        });
    }
};

export { requests };
