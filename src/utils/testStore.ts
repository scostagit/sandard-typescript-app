import checkPropTypes from 'check-prop-types';
import { applyMiddleware, createStore } from 'redux';
import rootReducer from '../store/reducers';
import createSagaMiddleware from 'redux-saga';

// create the saga middleware
const sagaMiddleware = createSagaMiddleware();

export const findByTestAtrr = (component: any, attr: any) => {
    const wrapper = component.find(`[data-test='${attr}']`);
    return wrapper;
};

export const checkProps = (component: any, expectedProps: any) => {
    const propsErr = checkPropTypes(
        component.propTypes,
        expectedProps,
        'props',
        component.name
    );
    return propsErr;
};

export const testStore = (initialState: any) => {
    const createStoreWithMiddleware = applyMiddleware(sagaMiddleware)(
        createStore
    );
    return createStoreWithMiddleware(rootReducer, initialState);
};
