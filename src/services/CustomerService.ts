import { injectable, inject } from "inversify";
import "reflect-metadata";
import ICustomerService from "../interfaces/ICustomerService";

//Decorate your classe with @injectable
@injectable()
export default class CustomerService implements ICustomerService {
    GetCustomerName(id: number): string {
        //Retrieving date from some API
        return `Your id is ${id} and your name is Jonh`;
    }
}