export default interface BaseState {
    loading: boolean;
    errorMessage: string;
}