export default interface IBaseState {
    loading: boolean;
    errorMessage: string;
}