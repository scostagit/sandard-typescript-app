export default interface ICustomerService {
    GetCustomerName(id: number): string;
}