import IBaseState from '../IBaseState';
import UserModel from '../../models/UserModel';

export default interface IUserState extends IBaseState {
    user: UserModel;
}