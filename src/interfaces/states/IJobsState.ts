import IBaseState from '../IBaseState';
import JobModel from '../../models/JobModel';

export default interface IJobsState extends IBaseState {
    jobs: JobModel[];
}