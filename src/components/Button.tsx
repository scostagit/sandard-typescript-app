import * as React from 'react';

interface IButtonProps {
  id: string;
  type?: 'submit' | 'reset' | 'button';
  label: string;
  disabled?: boolean;
  onClick(e: React.MouseEvent<HTMLElement>): void;
}

const Button = ({
  type,
  disabled = false,
  id,
  onClick,
  label
}: IButtonProps): JSX.Element => {
  return (
    <button type={type} disabled={disabled} name={id} id={id} onClick={onClick}>
      {label}
    </button>
  );
};

export default Button;
