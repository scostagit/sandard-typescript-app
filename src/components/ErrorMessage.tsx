import * as React from 'react';

interface IProps {
  display?: boolean;
  message?: string;
}

const ErrorMessage = ({ display = false, message = '' }: IProps): any => {
  return display ? (
    <div className="loginErr" id="loginErr" style={{ color: 'red' }}>
      <div>{message}</div>
    </div>
  ) : null;
};

export default ErrorMessage;
