import React from 'react';

export interface IFieldEvents {
    onChange(e: React.ChangeEvent<HTMLInputElement>): void,
    onBlur(e: React.FocusEvent<HTMLInputElement>): void
}

export interface IField extends IFieldEvents {
    name: string,
    label: string,
    type: string,
    error: any,
    value: string | string[] | number;
}

const Field = ({ onChange, onBlur, name, label, type, value, error }: IField) => (
    <div>
        <label>{label}</label>
        <div>
            <input
                style={error ? { backgroundColor: '#ffe6e6' } : {}}
                name={name}
                id={name}
                placeholder={label}
                type={type}
                value={value}
                onChange={onChange}
                onBlur={onBlur}
            />
            {error && <span style={{ color: 'red' }}>{error}</span>}
        </div>
    </div>
);

export default Field;
