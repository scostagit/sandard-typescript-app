import React from 'react';
import { IFieldEvents } from '../Field';

export interface IUserFormEntry {
    initialValues: any,
    validate: any,
    onSubmit(values: any, currentErrors: any): void
}

export interface IUserForm extends IFieldEvents {
    values: any;
    touchedValues: any;
    errors: any;
    handleSubmit(event: React.FormEvent<HTMLFormElement>): void
}

const useForm = ({ initialValues, onSubmit, validate }: IUserFormEntry): IUserForm => {
    const [values, setValues] = React.useState(initialValues || {});
    const [touchedValues, setTouchedValues] = React.useState({});
    const [errors, setErrors] = React.useState({});

    const onChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        setValues({
            ...values,
            [name]: value
        });
    };

    const onBlur = (event: React.FocusEvent<HTMLInputElement>): void => {
        const target = event.target;
        const name = target.name;

        setTouchedValues({
            ...touchedValues,
            [name]: true
        });

        const currentErrors = validate(values);
        setErrors({
            ...errors,
            ...currentErrors
        });
    };

    const handleSubmit = (event: React.FormEvent<HTMLFormElement>): void => {
        event.preventDefault();
        const currentErrors = validate(values);
        setErrors({
            ...errors,
            ...currentErrors
        });

        let hasError = false;
        for (var property in currentErrors) {
            if (currentErrors[property] !== '') {
                hasError = true;
            }
        }
        onSubmit(values, hasError ? currentErrors : null);
    };

    return {
        values,
        touchedValues,
        errors,
        onChange,
        handleSubmit,
        onBlur
    };
};

export default useForm;


