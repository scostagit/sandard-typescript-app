import * as React from 'react';

interface TextFiledGroupProps {
  type: 'text' | 'password' | 'radio' | 'number' | 'checkbox' | 'date';
  name: string;
  label: string;
  value?: any;
  disabled?: boolean;
  onChange(e: React.ChangeEvent<HTMLInputElement>): void;
}

const TextFieldGroup = (props: TextFiledGroupProps): JSX.Element => (
  <>
    <label>{props.label}</label>
    <input
      type={props.type}
      name={props.name}
      id={props.name}
      value={props.value}
      onChange={props.onChange}
      disabled={props.disabled}
    />
  </>
);

export default TextFieldGroup;
