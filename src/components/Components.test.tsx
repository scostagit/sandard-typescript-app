import React from 'react';
import { shallow } from 'enzyme';
import { Field, Button } from './index';

const clickFn = jest.fn();

describe('Test UI components', () => {
  it('should render the input with the label', () => {
    const handleOnChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
      e.preventDefault();
    };

    const handleOnBlur = (e: React.FocusEvent<HTMLInputElement>): void => {
      e.preventDefault();
    };

    const component = shallow(
      <Field
        type="text"
        name="userName"
        label="User Name"
        value=""
        onChange={handleOnChange}
        onBlur={handleOnBlur}
        error=""
      />

    );
    expect(component).toMatchSnapshot();
  });

  it('button click should trigger an event function', () => {
    const component = shallow(
      <Button id="btnTest" type="submit" label="test" onClick={clickFn} />
    );
    component.find('button#btnTest').simulate('click');
    expect(clickFn).toHaveBeenCalled();
  });
});
