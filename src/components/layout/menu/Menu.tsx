import * as React from 'react';

const Menu = (): JSX.Element => (
  <ul>
    <li>Home</li>
    <li>About</li>
    <li>Jobs</li>
  </ul>
);

export default Menu;
