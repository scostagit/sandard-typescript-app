import * as React from 'react';

const LoadingComponent = (props: { display: boolean }): any => {
  return props.display ? <div style={{ textAlign: 'center' }}>Loading...</div> : null;
};

export default LoadingComponent;
