export default class JobModel {
    eFolderID: string = '';
    eFolderName: string = '';
    eCreationTime: string = '';
    eEntryTime: string = '';
    txtPRNNumber: string = '';
    txtVehicleRegistrationNumber: string = '';
    txtVehicleDriverName: string = '';
    txtVehicleModel: string = '';
    txtSupplierName: string = '';
    txtDropOffTime: string = '';
    txtVehicleVINNumber: string = '';

    constructor() {

    }
}