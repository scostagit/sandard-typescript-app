export default class UserModel {
    userName?: string = '';
    password?: string = '';
    name?: string = '';
    email?: string = '';
    isLogged?: boolean = false;

    constructor() {

    }
} 