import { requests } from '../utils/network/api';

const JobApi = {
    getAll: () => {
        return requests.get('http://localhost:4000/jobs');
    }
};

export default JobApi;
