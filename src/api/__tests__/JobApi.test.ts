
import JobApi from '../JobApi';
jest.mock('../JobApi');

const jobs = [{
    "eFolderID": "0900000000000000000000002502750",
    "eFolderName": "LPFR-UAT-1044352",
    "eCreationTime": "Thu Oct 25 2019 16:23:00 GMT+0100 (British Summer Time)",
    "eEntryTime": "Thu Oct 25 2019 11:23:00 GMT+0100 (British Summer Time)",
    "txtPRNNumber": "5566f0fc-e9be-484c-95d7-34c5a70aabc0",
    "txtVehicleRegistrationNumber": "EP107KA",
    "txtVehicleDriverName": "BILLA PATRICK",
    "txtVehicleModel": "A4 AVANT BUSINE",
    "txtSupplierName": "A.I.G.",
    "txtDropOffTime": "11:35",
    "txtVehicleVINNumber": "WAUZZZ8K1FA105332"
}];

describe('Test Job Api', () => {
    beforeAll(() => {
        JobApi.getAll = jest.fn().mockImplementation(() => Promise.resolve({ data: jobs }));
    });
    it('The method get should return a list of jobs', async () => {
        let result = await JobApi.getAll();
        expect(result.data.length).toBeGreaterThan(0);
    });
});
