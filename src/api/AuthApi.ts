import { requests } from '../utils/network/api';
//import md5 from 'md5';

const AuthApi = {
    login: (userName: string, password: string) => {
        return requests.post('/user/LoginApi', {
            userName: userName,
            password: password //md5(password)
        });
    },

    logout: () => {
        //return requests.post('/user/logout');
    }
};

export default AuthApi;
