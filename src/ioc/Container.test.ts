import Container from "./Container";
import ICustomerService from "../interfaces/ICustomerService";
const costumerService = Container.get<ICustomerService>("ICustomerService");

describe('Testing the D.I Container', () => {
    it('The GetCustomerName should return the name.', () => {
        expect(costumerService.GetCustomerName(1)).toEqual("Your id is 1 and your name is Jonh");
    });

    it('The GetCustomerName should be not empty', () => {
        expect(costumerService.GetCustomerName(1)).not.toEqual('');
    });
});

