import { Container as ContainerDI } from "inversify";
//Import All injectble classes and interfaces
import ICustomerService from "../interfaces/ICustomerService";
import CustomerService from "../services/CustomerService";

//Creation of the Container
const Container = new ContainerDI();

//Register the Injectable classes and Interfaces
Container.bind<ICustomerService>("ICustomerService").to(CustomerService);

//returning the Container
export default Container;