﻿'use strict';
const path = require('path');
const WebpackNotifierPlugin = require('webpack-notifier');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const imageInlineSizeLimit = parseInt(process.env.IMAGE_INLINE_SIZE_LIMIT || '10000');
const mode = process.env.NODE_ENV || 'development';
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');

const rules = [
	{ test: /\.tsx?$/, loader: 'awesome-typescript-loader' },
	{
		test: /\.js$/,
		exclude: /node_modules/,
		use: {
			loader: 'babel-loader'
		}
	},
	{
		test: /\.css$/,
		use: [ MiniCssExtractPlugin.loader, 'css-loader' ]
	},
	{
		test: [ /\.bmp$/, /\.gif$/, /\.jpe?g$/, /\.png$/ ],
		loader: require.resolve('url-loader'),
		options: {
			limit: imageInlineSizeLimit,
			// name: 'static/media/[name].[hash:8].[ext]',
			name: 'images/[name].[ext]'
		}
	}
];

const plugins = [
	new WebpackNotifierPlugin(),
	new BrowserSyncPlugin(),
	new HtmlWebpackPlugin({
		template: './public/index.html'
	}),
	new MiniCssExtractPlugin({
		filename: '[name].css'
	}),
	new OptimizeCSSAssetsPlugin({})
];

const optimization = {
	splitChunks: {
		cacheGroups: {
			default: false,
			vendors: false,
			// vendor chunk
			vendor: {
				// name of the chunk
				name: 'vendor',
				// async + async chunks
				chunks: 'all',
				// import file path containing node_modules
				//test: /node_modules/,
				// priority
				priority: 20
			},
			// common chunk
			common: {
				name: 'common',
				minChunks: 2,
				chunks: 'all',
				priority: 10,
				reuseExistingChunk: true,
				enforce: true
			}
		}
	}
};

module.exports = {
	entry: {
		main: './src/index.tsx'
		//jobs: './src/pages/jobs/index.js',
		// vendor: vendorPackages
	},
	output: {
		path: path.resolve(__dirname, './build'),
		filename: '[name].js',
		chunkFilename: '[name].js',
		sourceMapFilename: '[file].map'
	},
	resolve: { extensions: [ '.js', '.jsx', '.react.js', '.ts', '.tsx' ] },
	module: {
		rules
	},
	mode,
	devtool: mode === 'development' ? 'inline-source-map' : false,
	plugins,
	optimization,
	performance: {
		hints: false,
		maxEntrypointSize: 512000,
		maxAssetSize: 512000
	}
};
